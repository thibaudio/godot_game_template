extends Node

@onready var audio_players: = $AudioPlayers
@onready var music_player = $MusicPlayer

signal change_master_volume
signal change_effects_volume
signal change_music_volume

func _ready():
	OptionsController.connect("options_loaded", Callable(self, "_on_options_loaded"))
	connect("change_master_volume", Callable(self, "_on_master_volume_changed"))
	connect("change_effects_volume", Callable(self, "_on_effects_volume_changed"))
	connect("change_music_volume", Callable(self, "_on_music_volume_changed"))

func play_sound(sound):
	for player in audio_players.get_children():
		if not player.playing:
			player.stream = sound
			player.play()
			break

func _on_master_volume_changed(volume):
	AudioServer.set_bus_volume_db(0, lerp(-80, 6, volume))
func _on_effects_volume_changed(volume):
	AudioServer.set_bus_volume_db(2, lerp(-80, 6, volume))
func _on_music_volume_changed(volume):
	AudioServer.set_bus_volume_db(1, lerp(-80, 6, volume))

func _on_options_loaded(options):
	_on_master_volume_changed(options.MasterVolume)
	_on_effects_volume_changed(options.EffectsVolume)
	_on_music_volume_changed(options.MusicVolume)
