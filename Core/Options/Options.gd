extends Resource
class_name Options

@export var MasterVolume: float = 100.0
@export var EffectsVolume: float = 80.0
@export var MusicVolume: float = 70.0

func serialize() -> String:
	var data := {
		"volume": {
			"master": MasterVolume,
			"effects": EffectsVolume,
			"music": MusicVolume
		}
	}
	
	return JSON.stringify(data)

func deserialize(json_string) -> void:
	var json = JSON.new()
	var error = json.parse(json_string)
	if error == OK:
		var data_received = json.data
		if typeof(data_received) == TYPE_DICTIONARY:
			MasterVolume = data_received.volume.master
			EffectsVolume = data_received.volume.effects
			MusicVolume = data_received.volume.music
		else:
			print("Unexpected data")
	else:
		print("JSON Parse Error: ", json.get_error_message(), " in ", json_string, " at line ", json.get_error_line())

