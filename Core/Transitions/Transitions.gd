extends CanvasLayer

@onready var animation_player: = $AnimationPlayer

signal transition_completed

func play_exit_transition():
	
	animation_player.play("ExitLevel")
	
func play_enter_transition():
	animation_player.play("EnterLevel")

func _on_AnimationPlayer_animation_finished(_anim_name):
	emit_signal("transition_completed")

func change_level(level):
	play_exit_transition()
	get_tree().paused = true
	await self.transition_completed
	play_enter_transition()
	get_tree().paused = false
	get_tree().change_scene_to_file(level)
