# Simple project template for Godot

## Usage
### Installation
Create a new repository from this one.  
Run `git lfs install`  

### SoundPlayer
For each sounds effects, add to `SoundPlayer.gd`:
```gdscript
const var SOUND_NAME = preload("<Sound_Path>")
```

To play the sound, in any scripts do:
```
SoundPlayer.play(SoundPlayer.SOUND_NAME)
```

To add music, just add the correct music file to SoundPlayer/MusicPlayer.

### Transitions
In any script, add:
```gdscript
Transitions.change_level(LEVEL_PATH)
```

## Includes
- [X] git setup
- [X] SoundPlayer
- [ ] Library Save
- [ ] Library Menu
- [X] Library Options
- [X] Library Transitions


## Credits
A lot of this was built from [Heartbeast platformer tutorial](https://www.youtube.com/watch?v=f3WGFwCduY0&list=PL9FzW-m48fn16W1Sz5bhTd1ArQQv4f-Cm)